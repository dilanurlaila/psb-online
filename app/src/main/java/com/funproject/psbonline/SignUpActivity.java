package com.funproject.psbonline;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.util.Patterns;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import butterknife.ButterKnife;
import butterknife.InjectView;

/**
 * Created by nurlaila on 8/8/2016.
 */
public class SignUpActivity extends AppCompatActivity {


    private static final String  TAG = "SignupAcitivity";

    @InjectView(R.id.etNameSignUp) EditText _nameSignup;
    @InjectView(R.id.etEmailSignUp) EditText _emailSignup;
    @InjectView(R.id.etPwSignUp) EditText _pwSignup;
    @InjectView(R.id.etConfirmPwSignUp) EditText _confirmPw;
    @InjectView(R.id.etHPSignUp) EditText _nohpSignup;
    @InjectView(R.id.btnSignUp) Button _btnSignup;

    DatabaseHelper helper = new DatabaseHelper(this);
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);

        ButterKnife.inject(this);

        _btnSignup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                signup();
            }
        });


    }

    public void signup(){
        Log.d(TAG, "SignUp");

        if(!validate()){
            onSignupFailed();
            return;
        }

        _btnSignup.setEnabled(false);

        final ProgressDialog progressDialog = new ProgressDialog(SignUpActivity.this,
                R.style.MyTheme);
        progressDialog.setMessage("Tunggu");
        progressDialog.show();

        String name = _nameSignup.getText().toString();
        String email = _emailSignup.getText().toString();
        String password = _pwSignup.getText().toString();
        String confirmpw = _confirmPw.getText().toString();
        int noHP = _nohpSignup.getText().length();

        //TODO: Implement signup and logic

        new android.os.Handler().postDelayed(

                new Runnable() {
                    @Override
                    public void run() {
                        onSignupSuccess();
                        progressDialog.dismiss();
                        

                    }
                }, 3000);
    }
    public void onSignupSuccess () {
        _btnSignup.setEnabled(true);
        setResult(RESULT_OK, null);
            Intent i = new Intent(SignUpActivity.this, BerandaActivity.class);
            startActivity(i);
    }

    public void onSignupFailed() {
        Toast.makeText(getBaseContext(), "Pastikan Yang Anda isikan Benar", Toast.LENGTH_SHORT).show();

        _btnSignup.setEnabled(true);
    }

    public boolean validate () {
        boolean valid = true;

        String name = _nameSignup.getText().toString();
        String email = _emailSignup.getText().toString();
        String password = _pwSignup.getText().toString();
        String confirmPw = _confirmPw.getText().toString();
        String noHP = _nohpSignup.getText().toString();

        if (name.isEmpty() || name.length() < 3) {
            _nameSignup.setError("lebih dari 3 karakter");
            valid = false;
        } else {
            _nameSignup.setError(null);
        }
        if (email.isEmpty() || !Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
            _emailSignup.setError("Masukkan Email valid");
            valid = false;
        } else {
            _emailSignup.setError(null);
        }
        if (password.isEmpty() || password.length() < 5 || password.length() > 10) {
            _pwSignup.setError("masukkan minimal 5 karakter");
            valid = false;
        } else {
            _pwSignup.setError(null);
        }
        //insert details in database
        {
            Contact c = new Contact();
            c.setUsername(name);
            c.setEmail(email);
            c.setPassword(password);
            c.setNoHP(noHP);

            helper.insertContact(c);
        }


        return valid;
    }


}
