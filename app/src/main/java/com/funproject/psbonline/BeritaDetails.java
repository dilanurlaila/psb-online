package com.funproject.psbonline;

/**
 * Created by rosli on 26/08/2016.
 */
public class BeritaDetails {
    int icon_berita;
    String from_berita;
    String judul;
    String isi;
    String time;

    public String getName(){
        return from_berita;
    }

    public void setName(String from_berita){
        this.from_berita = from_berita;
    }

    public String getJudul() {
        return judul;
    }

    public void setJudul(String judul) {
        this.judul = judul;
    }

    public int getIcon_berita() {
        return icon_berita;
    }

    public void setIcon_berita(int icon_berita) {
        this.icon_berita = icon_berita;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getIsi() {
        return isi;
    }

    public void setIsi(String isi) {
        this.isi = isi;
    }
}
