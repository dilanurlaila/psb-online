package com.funproject.psbonline;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.AdapterView;
import android.widget.ListView;

import java.util.ArrayList;

/**
 * Created by akuhkiki3 on 8/9/2016.
 */
public class JadwalActivity extends AppCompatActivity {

    ListView msgList;
    ArrayList<MessageDetails> details;
    AdapterView.AdapterContextMenuInfo info;

    @Override
    protected void onCreate(Bundle savedInstance) {
        super.onCreate(savedInstance);
        setContentView(R.layout.activity_jadwal);

        msgList = (ListView) findViewById(R.id.lv_item);

        details = new ArrayList<MessageDetails>();

        MessageDetails Detail;
        Detail = new MessageDetails();
        Detail.setIcon(R.drawable.ic_akun);
        Detail.setName("Jalur Reguler");
        Detail.setSub("Jadwal Test Gelombang I");
        Detail.setDesc("Senin, 29 Agustus 2016 07:00" +
        "\n" + "Selasa, 30 Agustus 2016  07:00");
        Detail.setTime("22/08/2016 12:23");
        details.add(Detail);

        Detail = new MessageDetails();
        Detail.setIcon(R.drawable.ic_akun);
        Detail.setName("Pengumuman");
        Detail.setSub("Ini Pengumuman");
        Detail.setDesc("Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. ");
        Detail.setTime("1/1/2016 00:00");
        details.add(Detail);

        msgList.setAdapter(new CustomAdapter(details, this));

    }

}