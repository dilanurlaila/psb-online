package com.funproject.psbonline;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

public class FormPendaftaranActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.form_pendaftaran);


        Button btn = (Button)findViewById(R.id.btn_berikut);
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(FormPendaftaranActivity.this, FormPendaftaran2Activity.class);
                startActivity(intent);
            }
        });

    }
    
}

