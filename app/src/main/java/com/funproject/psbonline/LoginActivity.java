package com.funproject.psbonline;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.util.Patterns;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import butterknife.ButterKnife;
import butterknife.InjectView;

/**
 * Created by rosli on 31/07/2016.
 */
public class LoginActivity extends AppCompatActivity {
    DatabaseHelper helper = new DatabaseHelper(this);
    private static final String TAG = "LoginActivity";
    private static final int REQUEST_ForgotPassword = 0;


    @InjectView(R.id.edtEmailLogin)
    EditText _email;
    @InjectView(R.id.edtPwLogin)
    EditText _password;
    @InjectView(R.id.forgotPw)
    TextView _ForgotPw;
    @InjectView(R.id.btnLogin)
    Button _btnLogin;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        ButterKnife.inject(this);

        _btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                login();
            }
        });

        _ForgotPw.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(), LoginActivity.class);
                startActivityForResult(intent, REQUEST_ForgotPassword);
            }
        });

    }

    public void login() {
        Log.d(TAG, "Login");

        if (!validate()) {
            onLoginFailed();
            return;
        }
        _btnLogin.setEnabled(false);

        final ProgressDialog  progressDialog= new ProgressDialog(LoginActivity.this,
                R.style.MyTheme);
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage("Loading");
        progressDialog.show();

        String email = _email.getText().toString();
        String password = _password.getText().toString();


        new android.os.Handler().postDelayed(
                new Runnable() {
                    @Override
                    public void run() {
                        onLoginSuccess();

                        progressDialog.dismiss();
                    }
                }, 3000);
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_ForgotPassword) {
            if (resultCode == RESULT_OK) {
                this.finish();
            }
        }

    }

    public void onLoginSuccess() {
        _btnLogin.setEnabled(true);
        EditText EmailLogin = (EditText) findViewById(R.id.edtEmailLogin);
        String str = EmailLogin.getText().toString();
        EditText PasswordLogin = (EditText) findViewById(R.id.edtPwLogin);
        String pass = PasswordLogin.getText().toString();

        String Password = helper.searchPass(str);
        if (pass.equals(Password)) {
            Intent i = new Intent(LoginActivity.this, BerandaActivity.class);
            i.putExtra("username", str);
            startActivity(i);
        } else {
            Toast temp =  Toast.makeText(LoginActivity.this, "Email dan Password tidak cocok", Toast.LENGTH_LONG);
            temp.show();
        }
    }


    public void onLoginFailed() {
        Toast.makeText(getBaseContext(), "Gagal Masuk", Toast.LENGTH_SHORT).show();

        _btnLogin.setEnabled(true);
    }

    public boolean validate() {
        boolean valid = true;

        String email = _email.getText().toString();
        String password = _password.getText().toString();

        if (email.isEmpty() || !Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
            _email.setError("masukkan email valid");
            valid = false;
        } else {
            _email.setError(null);
        }

        return valid;
    }

}