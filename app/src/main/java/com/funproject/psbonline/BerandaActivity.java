package com.funproject.psbonline;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.AdapterView;
import android.widget.ListView;

import java.util.ArrayList;

public class BerandaActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    ListView msgListBerita;
    ArrayList<BeritaDetails> details;
    AdapterView.AdapterContextMenuInfo info;
    public static final String PREFS_NAME = "LoginPrefs";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_beranda);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        msgListBerita = (ListView) findViewById(R.id.lv_item_beranda);

        details = new ArrayList<BeritaDetails>();

        BeritaDetails detail;
        detail = new BeritaDetails();
        detail.setIcon_berita(R.drawable.ic_akun);
        detail.setName("Selamat Datang di Aplikasi PSB Online");
        detail.setJudul("SELAMAT DATANG di SIAP Penerimaan Peserta Didik Baru (PPDB) Online periode 2016/2017");
        detail.setIsi("Aplikasi ini dipersiapkan sebagai pusat informasi dan pendaftaran siswa peserta PPDB periode 2016/2017 secara online real time process. Informasi lengkap seputar pelaksanaan PPDB akan di update di aplikasi ini. Bagi masyarakat dan calon siswa dapat memanfaatkan fasilitas Pesan Anda di situs ini untuk bantuan informasi lebih lanjut. Bagi anda calon peserta, harap membaca Aturan dan Prosedur pendaftaran dengan seksama sebelum melakukan proses pendaftaran.\n" +
                "\n" +
                "Demikian informasi ini dan terima kasih atas perhatian dan kerjasamanya.\n" +
                "\n" +
                " ");
        detail.setTime("22/08/2016 12:23");
        details.add(detail);

        detail = new BeritaDetails();
        detail.setIcon_berita(R.drawable.ic_akun);
        detail.setName("Notifikasi");
        detail.setJudul("Deskripsi singkat");
        detail.setIsi("Isi Pengumuman");
        detail.setTime("22/08/2016 12:23");
        details.add(detail);

        msgListBerita.setAdapter(new CustomAdapterBerita(details, this));

    }


    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            moveTaskToBack(true);
        }
    }



/*    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }*/

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_pendaftaran) {
            Intent intent = new Intent (BerandaActivity.this, FormPendaftaranActivity.class);
            startActivity(intent);
        } else if (id == R.id.nav_jadwal) {
            Intent intent = new Intent(BerandaActivity.this, JadwalActivity.class);
            startActivity(intent);
        } else if (id == R.id.nav_persyaratan) {
            Intent intent = new Intent(BerandaActivity.this, persyaratan.class);
            startActivity(intent);
        }else if (id == R.id.nav_Alur) {
            Intent intent = new Intent (BerandaActivity.this, AlurActivity.class);
            startActivity(intent);
        } else if (id == R.id.nav_akun) {
            Intent intent = new Intent (BerandaActivity.this, PofileActivity.class);
            startActivity(intent);

         } else if (id == R.id.nav_logout) {
            Intent logoutintent = new Intent(this, MainActivity.class);
            startActivity(logoutintent);
            SharedPreferences loginSharedPreferences;
            loginSharedPreferences = getSharedPreferences(PREFS_NAME, 0);
            SharedPreferences.Editor editor = loginSharedPreferences.edit();
            editor.remove("Logged");
            editor.commit();
            finish();

        }
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
}
