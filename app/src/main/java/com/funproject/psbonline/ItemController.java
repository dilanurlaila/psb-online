package com.funproject.psbonline;

import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.funproject.psbonline.R;


public class ItemController extends RecyclerView.ViewHolder implements View.OnClickListener {

    //Variable
    CardView cardItemLayout;
    TextView title;
    TextView subTitle;



    public ItemController(View itemView) {
        super(itemView);

        //set id
        cardItemLayout = (CardView) itemView.findViewById(R.id.cardlist_item);
        title = (TextView) itemView.findViewById(R.id.listitem_name);
        subTitle = (TextView) itemView.findViewById(R.id.listitem_subname);

        //onClick
        itemView.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        //tampilkan toas ketika click
        Toast.makeText(v.getContext(),
                String.format("Position %d", getAdapterPosition()),
                Toast.LENGTH_SHORT).show();
    }
}