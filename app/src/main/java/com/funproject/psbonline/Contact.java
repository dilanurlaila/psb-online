package com.funproject.psbonline;

/**
 * Created by nurlaila on 8/9/2016.
 */
public class Contact {

    private String email, username, password, NoHP;

    public void setEmail(String email){
        this.email=email;
    }
    public String getEmail() {
        return email;
    }

    public String getUsername(){
        return username;
    }
    public void setUsername(String username){
        this.username=username;

    }
    public String getPassword(){
        return password;
    }
    public void setPassword(String password){
        this.password=password;
    }

    public String getNoHP () {
        return NoHP;
    }
    public void setNoHP (String NoHP){
        this.NoHP=NoHP;
    }
}

