package com.funproject.psbonline;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by rosli on 26/08/2016.
 */
public class CustomAdapterBerita extends BaseAdapter {

    private ArrayList<BeritaDetails> _data;
    Context _c;

    CustomAdapterBerita (ArrayList<BeritaDetails> data, Context c){
        _data = data;
        _c = c;
    }

    public int getCount() {
        return _data.size();
    }

    public Object getItem(int position){
        return _data.get(position);
    }

    public long getItemId(int position){
        return position;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        View v = convertView;
        if (v == null) {
            LayoutInflater vi = (LayoutInflater) _c.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            v = vi.inflate(R.layout.list_item_berita, null);
        }
        ImageView image = (ImageView) v.findViewById(R.id.icon_berita);
        TextView fromView = (TextView) v.findViewById(R.id.From_berita);
        TextView subView = (TextView) v.findViewById(R.id.judul);
        TextView descView = (TextView) v.findViewById(R.id.isi);
        TextView timeView = (TextView) v.findViewById(R.id.time_berita);

        BeritaDetails msg = _data.get(position);
        image.setImageResource(msg.icon_berita);
        fromView.setText(msg.from_berita);
        subView.setText("Subject: " + msg.judul);
        descView.setText(msg.isi);
        timeView.setText(msg.time);
        return v;



    }
}
