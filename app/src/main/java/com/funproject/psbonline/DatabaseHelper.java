package com.funproject.psbonline;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by nurlaila on 8/10/2016.
 */
public class DatabaseHelper extends SQLiteOpenHelper {

    private static final int DATABASE_VERSION =1;
    private static final String DATABASE_NAME = "Login.db";
    private static final String TABLE_NAME="Login";
    private static final String COLUMN_ID="id";
    private static final String COLUMN_NAME = "name";
    private static final String COLUMN_EMAIL= "email";
    private static final String COLUMN_NOhp = "NOhp";
    private static final String COLUMN_PASS="pass";

    SQLiteDatabase db;
    public static final String TABLE_CREATE = "create table Login (id integer primary key not null , " +
            "name text not null , email text not null , NOhp number not null , pass text not null);";

    public DatabaseHelper(Context context){
        super(context, DATABASE_NAME , null , DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {

        db.execSQL(TABLE_CREATE);
        this.db = db;
    }

    public void insertContact(Contact c) {
        db = this.getWritableDatabase();
        ContentValues values = new ContentValues();

        String query = "select * from Login";
        Cursor cursor = db.rawQuery(query, null);
        int count = cursor.getCount();

        values.put(COLUMN_ID , count);
        values.put(COLUMN_NAME , c.getUsername());
        values.put(COLUMN_EMAIL , c.getEmail());
        values.put(COLUMN_PASS , c.getPassword());
        values.put(COLUMN_NOhp, c.getNoHP());

        db.insert(TABLE_NAME, null, values);
        db.close();


    }

    public String searchPass (String email) {
        db = this.getReadableDatabase();
        String query = "select email, pass from " + TABLE_NAME;
        Cursor cursor = db.rawQuery(query, null);
        String EmailLogin, Password;
        Password = "not found";
        if (cursor.moveToFirst()) {
            do {
                EmailLogin = cursor.getString(0);

                if (EmailLogin.equals(email)) {
                    Password = cursor.getString(1);
                    break;
                }
            } while (cursor.moveToNext());
        }
        return Password;
    }
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
    String query = "DROP TABLE IF EXISTS " +TABLE_NAME;
        db.execSQL(query);
        this.onCreate(db);

    }


}
